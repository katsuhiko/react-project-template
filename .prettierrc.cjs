/**
 * @file Prettierの設定ファイル。
 * @version 1.0.0
 *
 * @tutorial 依存パッケージのインストールは以下のコマンドを実行する。
    yarn add -D 'prettier@2'
 */

module.exports = {
  /** 複数行のHTML要素の閉じ括弧を最後の行の末尾に挿入する。 */
  bracketSameLine: false,
  /** オブジェクトリテラルの括弧の間にスペースを挿入する。 */
  bracketSpacing: true,
  /** 改行コード。 */
  endOfLine: 'lf',
  /** 複数行のJSX要素の閉じ括弧を最後の行の末尾に挿入する。 */
  jsxBracketSameLine: false,
  /** JSX内でダブルクォーテーションの代わりにシングルクォーテーションを使用する。 */
  jsxSingleQuote: false,
  /** 1行の最大文字数。 */
  printWidth: 100,
  /** 文の最後にセミコロンを挿入する。 */
  semi: true,
  /** ダブルクォーテーションの代わりにシングルクォーテーションを使用する。 */
  singleQuote: true,
  /** 末尾のカンマのスタイル。 */
  trailingComma: 'es5',
  /** インデント1段階に対応する半角スペース数。 */
  tabWidth: 2,
  /** インデントとして半角スペースの代わりにタブを使用する。 */
  useTabs: false,
};
