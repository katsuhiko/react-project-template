/**
 * @file remarkの設定ファイル。
 * @version 1.0.0
 *
 * @tutorial 依存パッケージのインストールは以下のコマンドを実行する。
    yarn add -D 'remark@14'
 *
 * @tutorial VSCodeで使用中に変更を加えた場合は再起動しないとリロードされない。
 */

module.exports = {
  settings: {
    /** 順序なしリストの行頭記号。 */
    bullet: '-',
    /** コードブロックを常にバッククォーテーションかチルダで挟む。 */
    fences: true,
    /** リストのインデントサイズ。 */
    listItemIndent: 'one',
  },
};
