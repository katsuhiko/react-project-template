# react-project-template

React プロジェクトの設定です。

## 基本設定

### 設定ファイルをコピー

以下のファイルはプロジェクトで汎用的に使える設定なのでそのままコピーします。

```
|- .vscode
  |- extensions.json ( VSCodeの推奨拡張機能 )
  |- settings.json ( VSCodeの設定 )
|- .eslintrc.cjs ( ESLintの設定 )
|- .prettierrc.cjs ( Prettierの設定 )
|- .remarkrc.cjs ( remarkの設定 )
```

### 検証コマンドの追加

`package.json`に以下を追記します。
`scripts`プロパティが既に存在する場合はそこに追記してください。

```
  "scripts": {
    "lint": "eslint . && tsc --noemit"
  }
```

## 環境別の設定

### [Create React App](https://create-react-app.dev/)

`create-react-app`で作成したプロジェクトは以下の設定を行います。

#### ESLintプラグインを無効化

設定の競合を防ぐため、以下を`.env`ファイルに追記してCreate React AppのESLintプラグインを無効化します。

```
# Create React App
DISABLE_ESLINT_PLUGIN=true
```

#### 出力ディレクトリを除外

`yarn run build`の出力ディレクトリを除外するため、以下を`.eslintignore`ファイルに追記します。

```
# Create React App
build/
```

### [Cloud Functions for Firebase](https://firebase.google.com/products/functions)

#### 出力ディレクトリを除外

`yarn run build`の出力ディレクトリを除外するため、以下を`.eslintignore`ファイルに追記します。

```
# Cloud Functions for Firebase
functions/lib/
```
